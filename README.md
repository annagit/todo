# TodoApp

Este projeto foi gerado com [Angular CLI](https://github.com/angular/angular-cli) versão 8.3.21. 
Após clonar o repositório rode `npm install`

## Development server

Rode `ng serve` para um dev server. Navegue para `http://localhost:4200/`. A aplicação irá atualizar automaticamente quando efetuar alguma mudança nos arquivos.
